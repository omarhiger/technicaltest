<?php

namespace App\Enums;


class EmployeeRole
{
    const Junior = 'junior';
    const Senior = 'senior';
    const Manager = 'manager';
    const Director = 'director';


    public static function getValues(): array
    {
        return [
            self::Junior,
            self::Senior,
            self::Manager,
            self::Director,
        ];
    }

}
