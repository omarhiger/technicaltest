<?php

namespace App\Console\Commands;

use App\Enums\CallStatus;
use App\Models\Call;
use App\Services\CallDispatcher;
use Illuminate\Console\Command;

class SimulateCallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'call:simulate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Simulate incoming calls';

    /**
     * Execute the console command.
     */
    public function handle(CallDispatcher $dispatcher)
    {
        $queue = new \SplQueue();
        $calls = Call::all();
        foreach ($calls as $call) {
            echo "Generated a {$call->priority} priority call with ID: {$call->id}\n";

            // If there are no available employees, add the call to the queue
            if (!$dispatcher->dispatchCall($call)) {
                $dispatcher->changeCallStatus($call,CallStatus::Waiting);
                $queue->enqueue($call);
                echo "Added call with ID: {$call->id} to the queue.\n";
            }

            // If there are calls in the queue and an employee is available, dispatch the next call in the queue
            while (!$queue->isEmpty()) {
                $queuedCall = $queue->dequeue();
                $dispatcher->dispatchCall($queuedCall);
                echo "Dispatched call with ID: {$queuedCall->id} from the queue.\n";
            }

            sleep(rand(1, 5));
        }
    }

}
