<?php

namespace Database\Seeders;

use App\Models\Call;
use Illuminate\Database\Seeder;

class CallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $calls = json_decode(file_get_contents(database_path('calls.json')), true);
        foreach ($calls as $call) {
            Call::create($call);
        }
    }
}
