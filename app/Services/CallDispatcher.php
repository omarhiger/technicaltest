<?php
namespace App\Services;

use App\Enums\PriorityType;
use App\Models\Call;
use App\Models\Employee;
use App\Enums\CallStatus;

class CallDispatcher
{


    public function dispatchCall(Call $call): bool
    {
        // Define the roles in the order they should be tried for each priority level
        $roles = [
            'low' => ['junior', 'senior', 'manager'],
            'high' => ['manager', 'director']
        ];

        // Try to assign the call to an employee
        foreach ($roles[$call->priority] as $role) {
            $employee = Employee::where('role', $role)
                ->where('is_free', 1)
                ->first();

            if ($employee) {
                return $this->assignCall($call, $employee);
            }
        }

        // If no employee is found, return false
        return false;
    }

    private function assignCall(Call $call, Employee $employee): bool
    {
        $call->employee()->associate($employee);
        $this->changeCallStatus($call,CallStatus::InProgress);
        $this->ChangeEmployeeStatus($employee,false);

        return  $this->handleCall($call,$employee);
    }

    private function handleCall(Call $call , Employee $employee): bool
    {
        // Check if the call should be escalated
        if ($call->priority === PriorityType::Low && rand(0, 10) > 3) {
            $this->escalateCall($call);
            $this->ChangeEmployeeStatus($employee,true);

            echo "Call {$call->id} escalated to high priority\n";
            return false;
        }

        $this->ChangeEmployeeStatus($employee,true);
        $this->changeCallStatus($call,CallStatus::Completed);

        echo "Employee {$employee->name} finished handling call {$call->id}\n";
        return true;
    }

    private function escalateCall(Call $call): void
    {
        $call->update([
            'priority' => PriorityType::High,
            'status' => CallStatus::Waiting
        ]);
    }

    public function changeCallStatus(Call $call , $status): void
    {
        $call->status = $status;
        $call->save();
    }

    private function ChangeEmployeeStatus(Employee $employee,$value): void
    {
        $employee->is_free = $value;
        $employee->save();
    }
}

