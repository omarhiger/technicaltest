<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(mixed $employee)
 * @method static where(string $string, string $role)
 * @property mixed $is_free
 * @property mixed $id
 * @property mixed $name
 */
class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'role',
        'is_free'
    ];

    public function call(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Call::class);
    }

}
