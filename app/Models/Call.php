<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed|string $status
 * @property mixed $priority
 * @property mixed $id
 * @property mixed $employee
 */
class Call extends Model
{
    use HasFactory;

    protected $fillable = [
        'priority',
        'status',
        'employee_id',
    ];

    public function employee(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Employee::class);
    }

}
