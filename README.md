# Technical Test

## Setup and Running the Project

 Please, follow these steps to setup and run the project:

1. Clone the project: `git clone https://gitlab.com/omarhiger/technicaltest.git`
2. Install dependencies: Run `composer install` in the terminal.
3. Setup environment:
   - Copy `.env.example` and rename the copy to `.env`.
4. Seed the database: Run `php artisan migrate --seed` in the terminal (you should run local server).
5. Start handling calls: Run `php artisan call:simulate` in the terminal (for see full senario).
6. Testing: Run `php artisan test` in terminal (just to see if functions are passed)
## Code Review

Here are some key points for reviewing the code:

- The command to start handling calls is `php artisan call:simulate`. You can find this command in the path: `app -> Console -> Commands -> SimulateCallCommand`.
- Test class in the path : `tests -> Feature -> CallDispatchTest`
- Or you can test data  in two JSON files located at: `database -> calls.json` & `employees.json`.
- If you want to test other cases, you can change the data in the JSON files. After changing, please run `php artisan migrate:fresh --seed` and then rerun the command `php artisan call:simulate`.
- Enums are located in `app -> Enums`.

## Feedback

I am eager to receive your feedback and advice after you review the code. I believe your insights will be extremely valuable and will help me enhance my skills and understanding.

Thank you once again for this opportunity, and I look forward to hearing your thoughts.
