<?php

namespace App\Enums;

class PriorityType {
    const High = 'high';
    const Low = 'low';

    public static function getValues(): array
    {
        return [
            self::High,
            self::Low,
        ];
    }

}
