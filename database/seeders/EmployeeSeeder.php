<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $employees = json_decode(file_get_contents(database_path('employees.json')), true);
        foreach ($employees as $employee) {
            Employee::create($employee);
        }
    }
}
