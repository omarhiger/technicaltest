<?php

namespace App\Enums;

class CallStatus {
    const Waiting = 'waiting';
    const InProgress = 'in progress';
    const Completed = 'completed';

    public static function getValues(): array
    {
        return [
            self::Waiting,
            self::InProgress,
            self::Completed,
        ];
    }

}
