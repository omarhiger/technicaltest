<?php

namespace Tests\Feature;

use App\Enums\CallStatus;
use App\Models\Call;
use App\Models\Employee;
use App\Services\CallDispatcher;
use Tests\TestCase;

class CallDispatchTest extends TestCase
{
    protected CallDispatcher $dispatcher;

    protected function setUp(): void
    {
        parent::setUp();

        $this->dispatcher = new CallDispatcher();
    }

    public function testLowPriorityCallIsHandledByJuniorOrSenior()
    {
        $call = new Call(['priority' => 'low']);

        $this->dispatcher->dispatchCall($call);

        $this->assertContains($call->employee->role, ['junior', 'senior','manager']);
    }

    public function testHighPriorityCallIsHandledByManagerOrDirector()
    {
        $call = new Call(['priority' => 'high']);

        $handled = $this->dispatcher->dispatchCall($call);

        $this->assertTrue($handled);
        $this->assertContains($call->employee->role, ['manager', 'director']);
    }


    public function testCallIsEscalatedWhenNecessary()
    {
        $call = new Call(['priority' => 'low']);

        $handled = $this->dispatcher->dispatchCall($call);

        if (!$handled) {
            $this->assertEquals('high', $call->priority);
        }
    }

    public function testCallIsAddedToQueueWhenNoEmployeesAreAvailable()
    {
        // Create enough calls to occupy all employees
        $calls = array_fill(0, count(Employee::all()) + 1, new Call(['priority' => 'low']));

        foreach ($calls as $call) {
            $handled = $this->dispatcher->dispatchCall($call);

            if (!$handled) {
                $this->assertEquals(CallStatus::Waiting, $call->status);
            }
        }
    }
}
